---
- name: ensure copy directory exists
  file:
    path: /home/docker/{{stack_name}}
    recurse: yes
    state: directory

- name: template out the environment variables
  template:
    src: ../templates/{{stack_name}}/env.j2
    dest: /home/docker/{{stack_name}}/env.txt
    owner: root
    group: root
    mode: 0644

- name: copy the docker-compose file
  copy:
    src: ../templates/{{stack_name}}/docker-compose.yml
    dest: /home/docker/{{stack_name}}/docker-compose.yml
    owner: root
    group: root
    mode: 0644

- name: ensure config directory exists
  file:
    path: "/mnt/config/{{ stack_name }}"
    owner: root
    group: root
    state: directory
    mode: 0755

- set_fact:
    supporting_templates: "{{ lookup('fileglob', '../templates/{{stack_name}}/*.j2', wantlist=true) | reject('search', 'env.j2') | list }}"

- name: copy jinja2 template files
  template:
    src: "{{ item }}"
    dest: /mnt/config/{{stack_name}}/{{ item | basename | replace('.j2','') }}
    owner: root
    group: root
    mode: 0644
  with_items: 
    - "{{ supporting_templates }}"

- set_fact:
    supporting_files: "{{ lookup('fileglob', '../templates/{{stack_name}}/*', wantlist=true) | reject('search', '.j2, docker-compose.yml') | list }}"

- name: copy all other supporting files
  copy:
    src: "{{ item }}"
    dest: /mnt/config/{{stack_name}}/{{ item | basename }}
    owner: root
    group: root
    mode: 0644
  with_items:
    - "{{ supporting_files }}"

- name: ensure the stack is started
  rancher_compose:
    docker_compose: /home/docker/{{stack_name}}/docker-compose.yml
    url: http://{{ rancher_server_host }}:{{ rancher_server_port }}
    access_key: "{{ rancher_server_access_key }}"
    secret_key: "{{ rancher_server_secret_key }}"
    env_file: /home/docker/{{stack_name}}/env.txt
    state: reloaded